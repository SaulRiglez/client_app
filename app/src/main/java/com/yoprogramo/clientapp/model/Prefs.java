package com.yoprogramo.clientapp.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.yoprogramo.clientapp.constants.Commons;

/**
 * Created by User on 2/1/2018.
 */

public class Prefs {


    private static final String TAG = Prefs.class.getSimpleName();

    private static Prefs instance;

    @SuppressWarnings("WeakerAccess")
    protected SharedPreferences sharedPrefs;

    public Prefs(final Context context) {
        this.sharedPrefs = context.getSharedPreferences(Commons.MAIN_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static void init(final Context context) {
        if (context != null) {
            instance = new Prefs(context);
        }
    }

    public static Prefs getInstance() {
        if (instance == null) {
            Log.e(TAG, "Prefs instance is null, did you forget to call init?");
        }

        return instance;
    }


    public void setKeyPref(final String value) {
        sharedPrefs.edit().putString(Commons.KEY, value).apply();
    }

    public String getKeyPref(final String defaultValue) {
        return sharedPrefs.getString(Commons.KEY, defaultValue);
    }
}
