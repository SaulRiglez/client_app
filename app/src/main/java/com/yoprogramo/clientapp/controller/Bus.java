package com.yoprogramo.clientapp.controller;

import android.support.annotation.Nullable;

import com.yoprogramo.clientapp.event.Event;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by User on 1/22/2018.
 */

public class Bus {

    private Bus() {
    }

    public static void register(Object object) {
        EventBus.getDefault().register(object);
    }

    public static void unregister(Object object) {
        EventBus.getDefault().unregister(object);
    }

    private static void post(Event.BaseEvent event) {
        EventBus.getDefault().post(event);
    }

    private static void postSticky(Event.BaseEvent event) {
        EventBus.getDefault().postSticky(event);
    }

    public static void loginReceivedEvent(boolean isLoggedIn, @Nullable String msg) {
        Event event = new Event();
        Event.LoginEvent loginEvent = event.new LoginEvent(isLoggedIn, msg);
        post(loginEvent);
    }


    public static void sendPosition(int position) {
        Event event = new Event();
        Event.PositionEvent positionEvent = event.new PositionEvent(position);
        post(positionEvent);
    }
}
