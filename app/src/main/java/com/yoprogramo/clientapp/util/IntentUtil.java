package com.yoprogramo.clientapp.util;

import android.content.Context;
import android.content.Intent;

import com.yoprogramo.clientapp.view.activities.HomeActivity;

/**
 * Created by User on 1/22/2018.
 */

public final class IntentUtil {

    private IntentUtil() {
        //Hide Constructor
    }

    public static Intent createHomeActivityIntent(final Context context ) {
        //final Intent intent = new Intent(context, HomeActivity.class);

        final Intent intent = new Intent(context, HomeActivity.class);
        return intent;
    }

}
