package com.yoprogramo.clientapp.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;

import com.yoprogramo.clientapp.R;

/**
 * Created by User on 1/22/2018.
 */

public final class AlertDialogUtil {

    private AlertDialogUtil() {
        // Hide constructor
    }


    public static void showDialog(Context context, String title, String msg) {
        showDialog(context, title, msg, context.getResources().getString(R.string.Ok));
    }

    public static void showDialog(Context context, String title, String msg, String okMessage) {
        if (context != null) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);

            if (!TextUtils.isEmpty(title)) {
                builder.setTitle(title);
            }

            builder.setMessage(msg);
            builder.setPositiveButton(okMessage,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });

            final AlertDialog dialog = builder.create();
            showDialogIfActivityExists(dialog, context);

        }
    }

    private static void showDialogIfActivityExists(AlertDialog dialog, Context context) {
        if (context instanceof Activity) {
            if (!((Activity) context).isFinishing()) {
                dialog.show();
            }
        } else {
            dialog.show();
        }
    }

}
