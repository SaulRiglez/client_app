package com.yoprogramo.clientapp.util;

import android.content.Context;
import android.util.Base64;

import com.yoprogramo.clientapp.model.Prefs;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class EncryptionUtil {

    static final String TAG = EncryptionUtil.class.getCanonicalName();
    private static final String RSA_ECB_PKCSONE_PADDING = "RSA/ECB/PKCS1Padding";
    private static final String AES_CBC_PKCSFIVE_PADDING = "AES/CBC/PKCS5Padding";
    private static final String AES = "AES";
    private static final String RSA = "RSA";
    private static final int KEY_SIZE = 256;
    private static final int KEY_PAIR_SIZE = 1024;
    private static final String UTF_EIGHT = "UTF-8";
    private static SecretKey scrtKey;
    private static IvParameterSpec iv;
    private static KeyPair pair;
    private static Key publicKey;
    private static Key privateKey;

    Context context;

    public EncryptionUtil(Context context) {
        this.context = context;
        Prefs.init(this.context);
    }

    public static void setUpEncryption() throws NoSuchPaddingException, NoSuchAlgorithmException {
        scrtKey = generateAESKey(KEY_SIZE);
        storeKey(scrtKey);
        iv = getIV();
        pair = generateKeyPair();
        publicKey = pair.getPublic();
        privateKey = pair.getPrivate();

    }

    private static void storeKey(SecretKey scrtKey) {
        String savedKey = Base64.encodeToString(scrtKey.getEncoded(),Base64.NO_WRAP);
        if(Prefs.getInstance().getKeyPref("").isEmpty()){
            Prefs.getInstance().setKeyPref(savedKey);
        }

    }


    public static class EncryptionAES {


        private EncryptionAES() {
            //Hide constructor
        }

        public static String encrpyt(String plainText) throws GeneralSecurityException, IOException {
            final Cipher cipher = Cipher.getInstance(AES_CBC_PKCSFIVE_PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, getSavedKey(), iv);
            return Base64.encodeToString(cipher.doFinal(plainText.getBytes(UTF_EIGHT)), Base64.NO_WRAP);
        }

        private static SecretKey getSavedKey() {
            byte [] decodedKey = Base64.decode(Prefs.getInstance().getKeyPref(""),Base64.NO_WRAP);
            SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, AES);
            return originalKey;
        }

        public static String decrpyt(String cipherText)
                throws GeneralSecurityException, IOException {
            final Cipher cipher = Cipher.getInstance(AES_CBC_PKCSFIVE_PADDING);
            cipher.init(Cipher.DECRYPT_MODE, getSavedKey(), iv);
            return new String(cipher.doFinal(Base64.decode(cipherText, Base64.NO_WRAP)));
        }

    }


    public static class EncryptionRSA {

        private EncryptionRSA(){
            //Hide constructor
        }

        public static byte[] encryptRsa(String plainText) throws Exception {
            final Cipher cipher = Cipher.getInstance(RSA_ECB_PKCSONE_PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            return cipher.doFinal(plainText.getBytes(UTF_EIGHT));
        }


        public static String dencryptRsa(byte[] plainbytes) throws Exception {
            final Cipher cipher = Cipher.getInstance(RSA_ECB_PKCSONE_PADDING);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return new String(cipher.doFinal(plainbytes));
        }

    }

    public static SecretKey generateAESKey(int keysize) throws NoSuchAlgorithmException {
        final SecureRandom random = new SecureRandom();
        final KeyGenerator generator = KeyGenerator.getInstance(AES);
        generator.init(keysize, random);
        return generator.generateKey();
    }

    public static IvParameterSpec getIV() {
        if (iv == null) {
            byte[] ivByteArray = new byte[16];
            // populate the array with random bytes
            new SecureRandom().nextBytes(ivByteArray);
            iv = new IvParameterSpec(ivByteArray);
        }
        return iv;
    }

    public static SecretKey getKey() throws NoSuchAlgorithmException {
        if (scrtKey == null) {
            scrtKey = generateAESKey(256);
        }
        return scrtKey;
    }

    public static KeyPair generateKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance(RSA);
        kpg.initialize(KEY_PAIR_SIZE);
        KeyPair kp = kpg.genKeyPair();
        return kp;
    }

}
