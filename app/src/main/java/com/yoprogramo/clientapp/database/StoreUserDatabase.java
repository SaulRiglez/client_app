package com.yoprogramo.clientapp.database;


import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.yoprogramo.clientapp.util.EncryptionUtil;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

public final class StoreUserDatabase {

    private static final int DATABASE_VER = 1;
    public static final String DATABASE_NAME = "userManager.db";
    public static final String TABLE_USERS = "users";
    public static final String COLUMN_EMAIL = "EMAIL";
    public static final String COLUMN_PWD = "PASSWORD";
    public static final String COLUMN_NAME = "NAME";
    public static final String COLUMN_LASTNAME = "LASTNAME";
    public static String PASS_PHRASE = "!@ABC";

    private WeakReference<Context> context = new WeakReference<>(null);
    private static StoreUserDatabase sInstance;
    protected SQLiteDatabase db;
    private DBHelper dbHelper;


    private StoreUserDatabase() {
        //Hide constructo
    }

    private StoreUserDatabase(final Context context) {
        this.context = new WeakReference<>(context);
    }

    public static synchronized StoreUserDatabase getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new StoreUserDatabase(context.getApplicationContext());
        }
        return sInstance;
    }

    public StoreUserDatabase open() {
        dbHelper = new StoreUserDatabase.DBHelper(getContext());
        db = dbHelper.getWritableDatabase(PASS_PHRASE);
        return this;
    }

    public void close() {
        if (dbHelper != null) {
            dbHelper.close();
        }
    }

    private Context getContext() {
        return context != null && context.get() != null ? context.get() : null;
    }


    //CRUD Methods

    public void insert(String email) throws GeneralSecurityException, IOException {
        ContentValues values = new ContentValues();
        values.put(COLUMN_EMAIL, EncryptionUtil.EncryptionAES.encrpyt(email));
        db.insert(TABLE_USERS, null, values);
    }

    public void updateEmail(String oldEmail, String newEmail) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_EMAIL, newEmail);
        db.update(TABLE_USERS, values, COLUMN_EMAIL + "='" + oldEmail + "'", null);
    }

    public void deleteEmail(String email) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_EMAIL, email);
        db.delete(TABLE_USERS, COLUMN_EMAIL + "='" + email + "'", null);
    }


    public List<String> getEmail() throws GeneralSecurityException, IOException {

        List<String> emails = new ArrayList<>();
        if (db != null) {
            String query = String.format("SELECT " + COLUMN_EMAIL + " FROM '%s'", TABLE_USERS);
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    final String email = cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL));
                    emails.add(email);
                    cursor.moveToNext();
                }
        }
            cursor.close();
        }else {
            Log.d("Photo", "getEmail: " + "bd null");
        }

        return emails;
    }




    private static class DBHelper extends SQLiteOpenHelper {


        private static DBHelper instance;


        private static final String SQL_CREATE_TABLE_QUERY =
                "CREATE TABLE IF NOT EXISTS " + TABLE_USERS +
                        " (" + COLUMN_EMAIL + " TEXT PRIMARY KEY" + ", " +
                        COLUMN_PWD + " TEXT" + ", " +
                        COLUMN_NAME + " TEXT" + ", " +
                        COLUMN_LASTNAME + " TEXT" + " )";


        private static final String SQL_DELETE_TABLE_QUERY =
                "DROP TABLE IF EXISTS " + TABLE_USERS;

        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VER);
        }

        static public synchronized DBHelper getInstance(Context context) {
            if (instance != null)
                instance = new DBHelper(context);
            return instance;
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_TABLE_QUERY);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(SQL_DELETE_TABLE_QUERY);
            onCreate(db);
        }
    }

}
