package com.yoprogramo.clientapp.view.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yoprogramo.clientapp.R;

/**
 * Created by User on 1/29/2018.
 */

public class HomeActivityFragment extends Fragment {


    public static HomeActivityFragment newInstance() {
        return new HomeActivityFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_accounts, container, false);
        return view;
    }
}
