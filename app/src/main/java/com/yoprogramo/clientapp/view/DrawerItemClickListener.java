package com.yoprogramo.clientapp.view;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yoprogramo.clientapp.R;
import com.yoprogramo.clientapp.controller.Bus;
import com.yoprogramo.clientapp.view.fragments.AccountsFragment;
import com.yoprogramo.clientapp.view.fragments.HomeActivityFragment;
import com.yoprogramo.clientapp.view.fragments.MarketsFragment;

/**
 * Created by yoprogramo on 1/29/2018.
 */

public class DrawerItemClickListener implements ListView.OnItemClickListener {


    private final ActionBarDrawerToggle drawerToggle;
    AppCompatActivity activity;
    String[] title;
    DrawerLayout drawerLayout;
    ListView drawerList;


    public DrawerItemClickListener(AppCompatActivity activity, String[] titles, DrawerLayout drawerLayout, ListView drawerList) {
        this.activity = activity;
        this.title = titles;
        this.drawerLayout = drawerLayout;
        this.drawerList = drawerList;


        drawerToggle = new ActionBarDrawerToggle(activity,
                drawerLayout,
                R.string.open_drawer,
                R.string.close_drawer
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
//                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                invalidateOptionsMenu();
            }
        };

        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();


    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        selectItem(position);
        Bus.sendPosition(position);
    }

    public void selectItem(int position) {

        drawerList.setItemChecked(position, true);
        drawerLayout.closeDrawer(drawerList);
        Fragment fragment;

        switch (position) {
            case 1:
                fragment = MarketsFragment.newInstance();
                break;
            default:
                fragment = HomeActivityFragment.newInstance();

        }


        FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment, "visible_frgament");
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
        setActionbarTitle(position);
        drawerList.setItemChecked(position, true);

    }

    private void setActionbarTitle(int position) {
        if (title != null && activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setTitle(title[position]);
        }
    }

    public void setHomeIcon() {
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeButtonEnabled(true);
        activity
                .getFragmentManager()
                .addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                                                   @Override
                                                   public void onBackStackChanged() {

                                                       int currentPosition =0;
                                                       FragmentManager fragMan = activity.getFragmentManager();
                                                       Fragment fragment = fragMan.findFragmentByTag("visible_frgament");

                                                       if(fragment instanceof AccountsFragment){
                                                           currentPosition = 0;
                                                       }
                                                       if(fragment instanceof MarketsFragment){
                                                           currentPosition = 1;
                                                       }

                                                       setActionbarTitle(currentPosition);
                                                       drawerList.setItemChecked(currentPosition, true);

                                                   }
                                               }


                );

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return false;
    }

}


