package com.yoprogramo.clientapp.view.activities;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.yoprogramo.clientapp.R;
import com.yoprogramo.clientapp.event.Event;
import com.yoprogramo.clientapp.view.DrawerItemClickListener;

import org.greenrobot.eventbus.Subscribe;

public class HomeActivity extends BusListenerActivity {

    private String[] titles;
    private ListView drawerList;
    private DrawerLayout drawerLayout;
    private DrawerItemClickListener drawerItemClickListener;
    int currentPosition =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initNavigationDrawer();
        drawerItemClickListener = new DrawerItemClickListener(this, titles, drawerLayout, drawerList);
        setOnclickListener();
        drawerItemClickListener.setHomeIcon();

        if(savedInstanceState != null){
            currentPosition = savedInstanceState.getInt("position");
            drawerItemClickListener.selectItem(currentPosition);
        } else{
            drawerItemClickListener.selectItem(0);
        }
    }


    private void setOnclickListener() {
        drawerList.setOnItemClickListener(drawerItemClickListener);
    }

    private void initNavigationDrawer() {
        drawerList = ((ListView) findViewById(R.id.drawer));
        drawerLayout = findViewById(R.id.drawer_layout);
        titles = getResources().getStringArray(R.array.navigation_drawer_option);
        drawerList.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_activated_1,
                titles));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return drawerItemClickListener.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putInt("position", currentPosition);
    }

    @Subscribe
    public void onEvent(Event.PositionEvent event) {
        currentPosition = event.getPosition();
    }

}
