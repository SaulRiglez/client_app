package com.yoprogramo.clientapp.view.activities;

import android.support.v7.app.AppCompatActivity;

import com.yoprogramo.clientapp.controller.Bus;

public abstract class BusListenerActivity extends AppCompatActivity {

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_bus_listener);
//    }

    @Override
    protected void onStart() {
        super.onStart();
        Bus.register(this);
    }


    @Override
    protected void onStop() {
        Bus.unregister(this);
        super.onStop();

    }
}
