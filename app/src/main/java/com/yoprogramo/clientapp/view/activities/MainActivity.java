package com.yoprogramo.clientapp.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.yoprogramo.clientapp.FirebaseHandler;
import com.yoprogramo.clientapp.R;
import com.yoprogramo.clientapp.database.StoreUserDatabase;
import com.yoprogramo.clientapp.event.Event;
import com.yoprogramo.clientapp.util.AlertDialogUtil;
import com.yoprogramo.clientapp.util.EncryptionUtil;
import com.yoprogramo.clientapp.util.IntentUtil;

import net.sqlcipher.database.SQLiteDatabase;

import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.NoSuchPaddingException;

public class MainActivity extends BusListenerActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser user;
    LinearLayout loginView;
    private EditText etUserName;
    private EditText etPassword;
    private Button btnLogin;
    private String userName;
    private String pass;
    private boolean isloggedIn;
    private LinearLayout loginViewBase;
    private StoreUserDatabase userDataBase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_view_layout);
        validateSession();
        initViews();
        setOnClickListener();
        SQLiteDatabase.loadLibs(this);
        userDataBase = StoreUserDatabase.getInstance(this);
        initDataBase();
        initEncryption();
    }


    private void validateSession() {

    }

    private void initDataBase() {
        userDataBase.open();
    }

    private void initEncryption() {
        try {
            EncryptionUtil encryptionUtil = new EncryptionUtil(this);
            encryptionUtil.setUpEncryption();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


    private void setOnClickListener() {
        btnLogin.setOnClickListener(onClickListener);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (validateFormLogIn()) {
                initUserAuhtentication();
            }
        }
    };

    private void initViews() {
        etUserName = findViewById(R.id.eTuserName);
        etPassword = findViewById(R.id.eTpassword);
        btnLogin = findViewById(R.id.btnLogIn);
        loginView = findViewById(R.id.login_view);
        loginViewBase = findViewById(R.id.login_view_base);
    }

    private void initUserAuhtentication() {
        FirebaseHandler firebaseAuth = FirebaseHandler.getInstance();
        mAuth = firebaseAuth.getFirebaseInstance();
        obtainUsarData();
        if (validateFormLogIn()) {
            //Toast.makeText(this, "Login Succes", Toast.LENGTH_SHORT).show();
            firebaseAuth.signIn(userName, pass);
            if (isloggedIn) {
                user = firebaseAuth.getCurrentUser();
                userDataBase.open();
                try {
                    if(userDataBase.getEmail().size()==0) {
                        userDataBase.insert(user.getEmail());
                    }
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else {
            Toast.makeText(this, "Failure", Toast.LENGTH_SHORT).show();
        }
    }

    private void obtainUsarData() {
        userName = etUserName.getText().toString();
        pass = etPassword.getText().toString();
    }


    private boolean validateFormLogIn() {
        boolean valid = true;
        String email = etUserName.getText().toString();
        String password = etPassword.getText().toString();
        if (TextUtils.isEmpty(email)) {
            etUserName.setError("Required");
            valid = false;
        } else {
            etUserName.setError(null);
        }

        if (TextUtils.isEmpty(password)) {
            etPassword.setError("Required");
            valid = false;
        } else {
            etPassword.setError(null);
        }
        return valid;
    }

    @Subscribe
    public void onEvent(Event.LoginEvent event) {
        isloggedIn = event.isLoggedIn();
        if (isloggedIn) {
            Toast.makeText(this, "Logged In!", Toast.LENGTH_SHORT).show();
            Intent intent = IntentUtil.createHomeActivityIntent(this);
            startActivity(intent);
            finish();
        } else {
            AlertDialogUtil.showDialog(this,"Unable to Log On", event.getMsg());
        }
    }
}
