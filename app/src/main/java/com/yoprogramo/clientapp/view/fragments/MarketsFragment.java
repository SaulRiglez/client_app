package com.yoprogramo.clientapp.view.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yoprogramo.clientapp.R;

/**
 * Created by User on 1/28/2018.
 */

public class MarketsFragment extends Fragment {

    public MarketsFragment() {
        // Required empty public constructor
    }


    public static MarketsFragment newInstance(){
        return new MarketsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_markets, container, false);
        return view;
    }
}
