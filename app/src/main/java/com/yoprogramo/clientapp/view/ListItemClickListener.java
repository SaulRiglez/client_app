package com.yoprogramo.clientapp.view;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by User on 1/31/2018.
 */

public class ListItemClickListener implements ListView.OnItemClickListener {

    Context context;

    public ListItemClickListener(Context context) {
        this.context = context;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {

        view.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, "Desde el mas alla " + position, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
