package com.yoprogramo.clientapp;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.yoprogramo.clientapp.controller.Bus;


/**
 * Created by User on 1/18/2018.
 */

public class FirebaseHandler {

    private static FirebaseHandler instance;
    private FirebaseAuth mAuth;
    private static String TAG = "Photo";

    private FirebaseHandler() {
    }

    public static FirebaseHandler getInstance() {
        if (instance == null) {
            instance = new FirebaseHandler();
        }
        return instance;
    }

    public FirebaseAuth getFirebaseInstance() {
        mAuth = FirebaseAuth.getInstance();
        return mAuth;
    }

    public FirebaseUser getCurrentUser() {
        return mAuth.getCurrentUser();
    }

    public void signIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "onComplete: " + task.toString());
                        if (task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail: Success");
                            Bus.loginReceivedEvent(true, null);
                        } else {
                            Log.w(TAG, "signInWithEmail: failure", task.getException());
                            Bus.loginReceivedEvent(false, task.getException().getMessage());
                        }
                    }
                });

    }


}
