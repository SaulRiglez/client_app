package com.yoprogramo.clientapp.event;

/**
 * Created by User on 1/22/2018.
 */

public class Event {

    public class BaseEvent {

    }

    public class LoginEvent extends BaseEvent {

        boolean isLoggedIn;
        String msg;

        public LoginEvent(boolean isLoggedIn, String msg) {
            this.isLoggedIn = isLoggedIn;
            this.msg = msg;
        }

        public boolean isLoggedIn() {
            return isLoggedIn;
        }

        public String getMsg() {
            return msg;
        }
    }

    public class PositionEvent extends BaseEvent {
        int position;

        public PositionEvent(int position) {
            this.position = position;
        }

        public int getPosition() {
            return position;
        }
    }

}
